package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
     public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint =paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
    }

    @Override
    public Void onStroke(final Stroke c) {
        paint.setColor(c.getColor());
        Shape s=c.getShape();
        s.accept(this);
		paint.setColor(anyInt());
		return null;
	}

	private int anyInt() { return 100;
	}

	@Override
	public Void onFill(final Fill f) {
        paint.setStyle(f.getStyle());
        Shape s=f.getShape();
        s.accept(this);
		paint.setStyle(any());
		return null;
	}

	@Override
	public Void onGroup(final Group g) {

        for (Shape s:g.getShapes())
        {
        s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
			canvas.translate(l.getX(),l.getY());
        Shape s=l.getShape();
        s.accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
        canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
        paint.setStyle(o.getStyle());
        Shape s=o.getShape();
        s.accept(this);
		paint.setStyle(any());
		return null;
	}

	private Style any() { return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
        float [] pts=new float[100];
        int i=-1;
        for (Point p:s.getPoints())
        {i=i+1;
          pts[i]=p.getX();
			i=i+1;
			pts[i]=p.getY();
        }
       canvas.drawLines(pts,paint);
		return null;
	}
}
