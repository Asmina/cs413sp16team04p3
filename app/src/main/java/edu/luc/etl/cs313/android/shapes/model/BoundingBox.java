package edu.luc.etl.cs313.android.shapes.model;

import java.util.Iterator;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override

	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
        Shape s=f.getShape();
        Location loc=s.accept(this);
        return loc;
	}

	@Override
	public Location onGroup(final Group g) {
   int [] XMax=new int[50];
        int [] XMin=new int[50];
        int [] YMax=new int[50];
        int [] YMin=new int[50];
        int i=-1;
        int XFMax,YFMax,XFMin,YFMin;
        for (Shape s:g.getShapes())
        {
            i=i+1;
            Location loc=s.accept(this);
            XMin[i]=loc.getX();
            YMin[i]=loc.getY();
            XMax[i]=loc.getX()+((Rectangle)loc.getShape()).getWidth();
            YMax[i]=loc.getY()+((Rectangle)loc.getShape()).getHeight();
        }
        //find XFMax
        XFMax=XMax[0];
        for (int j=1;j<=i;j++)
        {
            if (XMax[j]>XFMax)
            {XFMax=XMax[j];}
        }

        //find XFMin
        XFMin=XMin[0];
        for (int j=1;j<=i;j++)
        {
            if (XMin[j]<XFMin)
            {XFMin=XMin[j];}
        }

        //find YFMax
        YFMax=YMax[0];
        for (int j=1;j<=i;j++)
        {
            if (YMax[j]>YFMax)
            {YFMax=YMax[j];}
        }

        //find YFMin
        YFMin=YMin[0];
        for (int j=1;j<=i;j++)
        {
            if (YMin[j]<YFMin)
            {YFMin=YMin[j];}
        }
        int width=XFMax-XFMin;
        int height=YFMax-YFMin;
        return new Location(XFMin,YFMin, new Rectangle(width,height));

	}

	@Override
	public Location onLocation(final Location l) {

        final int x1 = l.getX();
        final int y1 = l.getY();
        Shape s=l.getShape();
        Location loc=s.accept(this);
        int x=loc.getX();
        int y=loc.getY();

        return new Location(x+x1,y+y1,(Rectangle)loc.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {

        final int w = r.getWidth();
        final int h = r.getHeight();
        return new Location(0,0,new Rectangle(w,h));
	}

	@Override
	public Location onStroke(final Stroke c) {
        Shape s=c.getShape();
        Location loc=s.accept(this);
        return loc;
	}

	@Override
	public Location onOutline(final Outline o) {
        Shape s=o.getShape();
        Location loc=s.accept(this);
        return loc;
	}

	@Override
	public Location onPolygon(final Polygon s) {
        int[] x=new int[50];
        int[] y=new int[50];
        int i=-1;
        int XMax=0;
        int XMin=0;
        int YMax=0;
        int YMin=0;
        for (Point p:s.getPoints())
        {
            i=i+1;
           x[i] = p.getX();
            y[i] = p.getY();
        }

        //find XMax
        XMax=x[0];
        for (int j=1;j<=i;j++)
        {
            if (x[j]>XMax)
            {XMax=x[j];}
        }

        //find XMin
        XMin=x[0];
        for (int j=1;j<=i;j++)
        {
            if (x[j]<XMin)
            {XMin=x[j];}
        }

        //find YMax
        YMax=y[0];
        for (int j=1;j<=i;j++)
        {
            if (y[j]>YMax)
            {YMax=y[j];}
        }

        //find YMin
        YMin=y[0];
        for (int j=1;j<=i;j++)
        {
            if (y[j]<YMin)
            {YMin=y[j];}
        }
int width=XMax-XMin;
        int height=YMax-YMin;
        return new Location(XMin,YMin, new Rectangle(width,height));
	}
}
