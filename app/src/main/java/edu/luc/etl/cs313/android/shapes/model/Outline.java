package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Paint;

/**
 * A decorator indicating that a shape should be drawn as an outlined shape
 * instead of a filled one.
 */
public class Outline implements Shape {

	protected final Shape shape;
	protected final Paint.Style style;

	public Outline(final Shape shape, Paint.Style style) {
		this.shape = shape;
		this.style = style;
	}

	public Shape getShape() {
		return shape;
	}
	public Paint.Style getStyle() {
		return style;
	}

	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		// TODO your job
		return v.onOutline(this);
	}
}
