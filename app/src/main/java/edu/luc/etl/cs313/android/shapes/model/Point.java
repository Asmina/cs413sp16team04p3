package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A point, implemented as a location without a shape.
 */
public class Point extends Location {

	// TODO your job
	// HINT: use a circle with radius 0 as the shape!

	public Point(final int x, final int y) {
		super(x, y, null);
		assert x >= 0;
		assert y >= 0;
	}
    public int getX() {
        return super.getX();
    }
    public int getY() {
        return super.getY();
    }
	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		return v.onLocation(this);
	}
}
