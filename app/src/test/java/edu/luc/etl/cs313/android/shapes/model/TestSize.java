package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Paint;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static edu.luc.etl.cs313.android.shapes.model.Fixtures.*;

public class TestSize {

	protected Size v;

	@Before
	public void setUp() {
		v = new Size();
	}

	@After
	public void tearDown() {
		v = null;
	}

	@Test
	public void testCircle() {
		assertEquals(1, new Circle(50).accept(v).intValue());
	}

	@Test
	public void testRectangle() {
		assertEquals(1, new Rectangle(80, 120).accept(v).intValue());
	}

	@Test
	public void testLocation() {
		assertEquals(1, Fixtures.simpleLocation.accept(v).intValue());
	}

	@Test
	public void testFill() {
		assertEquals(1, Fixtures.simpleFill.accept(v).intValue());
	}

	@Test
	public void testStroke() {
		assertEquals(1, new Stroke(1,new Rectangle(80, 120)).accept(v).intValue());
	}

	@Test
	public void testGroupSimple() {
		assertEquals(2, Fixtures.simpleGroup.accept(v).intValue());
	}

	@Test
	public void testGroupComplex() {
		assertEquals(9, Fixtures.complexGroup.accept(v).intValue());
	}

	@Test
	public void testOutline() {
		assertEquals(1, new Outline(new Rectangle(80, 120), Paint.Style.STROKE).accept(v).intValue());
	}

	@Test
	public void testPolygon() {
		assertEquals(2, new Polygon(new Point(1,1), new Point(2,2)).accept(v).intValue());
	}
}
